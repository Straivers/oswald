module oswald.errors;

enum WindowError
{
    NoError,
    ParameterMismatch,
    WindowConstructionFailed,
    TitleTooLong,
}
